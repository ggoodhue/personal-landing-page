module.exports = (grunt) ->
  grunt.registerTask "default", [
    "sass"
    "newer:postcss:dev"
    "newer:imagemin"
    "newer:concat"
  ]
  grunt.registerTask "prod", [
    "sass"
    "newer:postcss:prod"
    "cssmin"
    "newer:concat"
  ]
  grunt.registerTask "compress", [
    "imagemin:imagesTMP"
    "responsive_images"
    "imagemin:images"
  ]
  # create a server and watch with live reload
  grunt.registerTask "serve-public", [
    "connect:public"
    "watch"
  ]  
  grunt.registerTask "serve-private", [
    "connect:private"
    "watch"
  ]
  # TO DO: open staging site after deployed
  grunt.registerTask "deploy-staging", "Deploy to the staging environment", ->
    grunt.task.run "prod"
    grunt.task.run "rsync:staging"
    returnKB
  
  # TO DO: open live production after deployed
  grunt.registerTask "deploy-public", "Deploy to the production environment", ->
    grunt.task.run "prod"
    grunt.task.run "rsync:public"  
    return

  grunt.registerTask "deploy-private", "Deploy to the private environment", ->
    grunt.task.run "prod"
    grunt.task.run "rsync:private"
    return

  return