module.exports =
	options:
		ignore: [
			"primary",
			"secondary",
			"tertiary",
			"quaternary",
			"quinary",
			"flexbox",
			"no-flexbox",
			"close",
			"open"
		]
	dist:
		files:
			"tmp/private.uncss.css": ["private/*.html", "public/index.html"]