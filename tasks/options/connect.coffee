module.exports =
  public:
    options:
      port: 3030,
      hostname: '0.0.0.0'
      base: 'public'
      livereload: true
      appName: 'open'
      target: 'http://localhost:3030'
  private:
    options:
      port: 3030,
      hostname: '0.0.0.0'
      base: 'private'
      livereload: true
      appName: 'open'
      target: 'http://localhost:3030'