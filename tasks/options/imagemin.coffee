module.exports =
	images:
		files: [
			expand: true
			cwd: "./tmp/_/img/"
			src: ["**/*.{jpg,jpeg,gif,png}"]
			dest: "./private/_/img/"
			]	
	imagesTMP:
		files: [
			expand: true
			cwd: "./src/img/"
			src: ["**/*.{jpg,jpeg,gif,png}"]
			dest: "./tmp/_/img/"
		]