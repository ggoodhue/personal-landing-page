module.exports =
	public:	
		options:
			cssmin: true
		src: 'tmp/index.html'
		dest: 'public/index.html'
	private:	
		options:
			cssmin: true
		src: '/tmp/private.*.html'
		dest: '/private/'