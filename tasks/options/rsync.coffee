module.exports =
  options:
    args: ["--verbose"]
    recursive: true
    exclude: [
      "package.json"
      "node_modules"
      "*.sublime-project"
      "*.sublime-workspace"
      "src"
      ".gitignore"
      ".sass-cache"
      ".git"
      "./src/img/*"
    ]
  public:
    options:
      src: "./public/"
      dest: '/home/godiswar/public_html/'
      host: 'godiswar@160.153.51.0'
  staging:
    options:
      src: "./public/"
      dest: '/home/godiswar/public_html/staging/'
      host: 'godiswar@160.153.51.0'
  private:
    options:
      src: "./private/"
      dest: '/home/godiswar/public_html/thevault/'
      host: 'godiswar@160.153.51.0'