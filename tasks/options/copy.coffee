module.exports =
	private:	
		files: [  
			expand: true
			cwd: "./src/img/"
			src: ["**/*.gif"]
			dest: './private/_/img/'
			filter: 'isFile'
		]