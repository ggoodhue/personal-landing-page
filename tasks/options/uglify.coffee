module.exports =
  dev:
    options:
      beautify: true
    files:
      "public/_/js/scripts.js": ["tmp/scripts.js"]
  prod:
    options:
      compress: {}
    files:
      "public/_/js/scripts.js": ["tmp/scripts.js"]
      "private/_/js/scripts.js": ["tmp/private.scripts.js"]