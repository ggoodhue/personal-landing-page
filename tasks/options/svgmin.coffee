module.exports =
  dist:
    files: [
      expand: true             
      cwd: "./src/img/"
      src: ["**/*.svg"]
      dest: "./private/_/img/"
    ]