module.exports =
  yaml:
    files: ["src/data/*.yml"]
  sass:
    files: ["src/scss/{**/,*}.scss"]
    tasks: ["sass", "autoprefixer:dev", "autoprefixer:privatedev", "inlinecss"]
  private: 
    files: ["src/pug/private.*.pug", "src/pug/**/*.pug"]
    tasks: ["pug:private", "responsive_images_extender", "inlinecss:private"]
  html:
    files: ["src/pug/index.pug", "src/pug/partials/*.pug"]
    tasks: ["pug", "newer:inlinecss:public"]
  coffee:
    files: ["src/js/coffee/*.coffee"]
    tasks: ["coffee", "newer:concat", "uglify:prod"]
  scripts:
    files: ["src/js/*.js"]
    tasks: ["newer:concat"]
  gif: 
    files: ["src/img/{**/,*}.gif"]
    options: ["added","changed"]
    tasks: ['newer:copy']
  imagemin:
    files: ["src/img/{**/*}.{png,jpg,jpeg}"]
    options: ["added","changed"]
    tasks: ["newer:imagemin"]
  svg:
    files: ["src/img/{**/,*}.svg"]
    options: ["added", "changed"]
    tasks: ["newer:svgmin"]
  livereload:
    files: ["public/{**/,*}", "private/{**/,*}"]
    options:
      livereload: true