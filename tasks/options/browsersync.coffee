module.exports = 
  bsFiles:
    src: [
      "public/*.css"
      "public/*.html"
      "public/js/*.js"
    ]
  options:
    watchTask: true
    open: true
    proxy: "0.0.0.0:3030"