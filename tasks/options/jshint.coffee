module.exports =
  options:
    curly: true
    eqeqeq: true
    eqnull: true
    browser: true
    globals:
      jQuery: true
  prod:
    files:
      src: [
        "public/_/js/*.js"
        "Gruntfile.js"
      ]
  dev:
    files:
      src: ["src/js/*.js"]