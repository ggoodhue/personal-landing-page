module.exports = 
  public:
    files:
      "tmp/index.html": "src/pug/index.pug"
      "public/index-eng.html": "src/pug/partials/intro-eng.pug"
      "public/index-deu.html": "src/pug/partials/intro-deu.pug"
  private:
    options:
      data: 
        data:require("quaff")("src/data")
      debug: true
    files:
        "private/inclusion.html": "src/pug/private.inclusion.pug"
        "private/mini-deloitte.html": "src/pug/private.mini-deloitte.pug"
        "private/systems-integrations.html": "src/pug/private.systems-integrations.pug"
        "private/kickstart.html": "src/pug/private.kickstart.pug"
        "private/family-hub.html": "src/pug/private.family-hub.pug"
        "private/mpol.html": "src/pug/private.mpol.pug"
        "private/deloitte-talent-platform.html": "src/pug/private.deloitte-talent-platform.pug"
        "private/cycling.html": "src/pug/private.cycling.pug"
        "private/triedandtrue.html": "src/pug/private.triedandtrue.pug"
        "private/product-watchlist.html": "src/pug/private.product-watchlist.pug"
        "private/datapoint-card.html": "src/pug/private.datapoint-card.pug"
        "private/searchproduct.html": "src/pug/private.searchproduct.pug"
        "private/intergames.html": "src/pug/private.intergames.pug"
        "private/404.html": "src/pug/404.pug"
        "private/index.html": "src/pug/private.index.pug"