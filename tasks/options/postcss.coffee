module.exports =
  options:
    map: true
    processors: require("autoprefixer") browsers:[ 'last 1 version' ]
  dev: 
    src: "tmp/style.css"
    dest: "public/style.css"
  prod:
    src: "tmp/style.css"
    dest: "tmp/style.auto.css"  
  privateprod:
    src: "tmp/private.style.css"
    dest: "tmp/style.auto.css"  
  privatedev:
    src: "tmp/private.style.css"
    dest: "private/style.css"