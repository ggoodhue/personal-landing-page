module.exports = 
  public:
    files:
      "tmp/index.html": "src/jade/index.jade"
      "public/index-eng.html": "src/jade/partials/intro-eng.jade"
      "public/index-deu.html": "src/jade/partials/intro-deu.jade"
  private:
    options:
      data:
        data: require("quaff")("src/data")
        debug: true
    files:
        "private/inclusion.html": "src/jade/private.inclusion.jade"
        "private/mini-deloitte.html": "src/jade/private.mini-deloitte.jade"
        "private/systems-integrations.html": "src/jade/private.systems-integrations.jade"
        "private/kickstart.html": "src/jade/private.kickstart.jade"
        "private/family-hub.html": "src/jade/private.family-hub.jade"
        "private/mpol.html": "src/jade/private.mpol.jade"
        "private/deloitte-talent-platform.html": "src/jade/private.deloitte-talent-platform.jade"
        "private/cycling.html": "src/jade/private.cycling.jade"
        "private/triedandtrue.html": "src/jade/private.triedandtrue.jade"
        "private/product-watchlist.html": "src/jade/private.product-watchlist.jade"
        "private/datapoint-card.html": "src/jade/private.datapoint-card.jade"
        "private/searchproduct.html": "src/jade/private.searchproduct.jade"
        "private/intergames.html": "src/jade/private.intergames.jade"
        "private/404.html": "src/jade/404.jade"
        "private/index.html": "src/jade/private.index.jade"