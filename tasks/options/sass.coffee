module.exports = 
  dev:
    files:
      "tmp/style.css": "src/scss/style.scss"
      "tmp/private.style.css": "src/scss/private.style.scss"
    options:
      style: "expanded"
      lineNumbers: true