module.exports = myTask:
  options: sizes: [
    {
      width: 320
    }
    {
      name: 'medium'
      width: 640
    }
    {
      name: 'large'
      width: 1024
      quality: 60
    }    
    {
      name: 'xlarge'
      width: 1400
      quality: 60
    }
  ]
  files: [ {
    expand: true
    cwd: "./src/img/"
    src: ["**/*.{jpg,jpeg,gif,png}"]
    dest: "./tmp/_/img/"
  } ]
