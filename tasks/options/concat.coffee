module.exports = 
  dev:
    src: [
      "src/js/modernizr-custom.js"
      "src/bower/headroom.js/dist/headroom.min.js"
      "src/js/site.js"
    ]
    dest: "tmp/scripts.js"  
  private:
    src: [
      "src/js/modernizr-custom.js"
      "src/bower/headroom.js/dist/headroom.min.js"
      "src/bower/scrollreveal/scrollreveal.js"
      "src/bower/smooth-scroll/dist/js/smooth-scroll.min.js"
      "src/bower/picturefill/dist/picturefill.min.js"
      "src/js/private.js"
    ]
    dest: "tmp/private.scripts.js"