# Personal Site

A landing page for my personal site on garretgoodhue.com and private.garretgoodhue.com

-----
## Getting Setup

* [Node](http://nodejs.org)
* [NPM](https://www.npmjs.org/): `brew install node`
* [Grunt](http://www.gruntjs.com): `npm install -g grunt-cli`

## Installing Dependencies

Run the following command to install any dependencies that the project requires

```
npm install
```
if you run into any issues try running `sudo npm install`

## Watching

Set up a local server and watch files

Public directory

```
grunt serve-public
```
Private directory
```
grunt serve-private
```
You can then view the files on 

```
http://localhost:3030/
```
## Compressing Files

To optimize these files a bit further for various viewports

```
grunt compress
```

## Deploying

Deploying to staging

```
grunt deploy-staging
```
Deploying to production or to garretgoodhue.com

```
grunt deploy-production
```
Deploying to private server which includes portfolio peices to private.garretgoodhue.com

```
grunt deploy-private
```
