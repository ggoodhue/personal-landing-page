(function() {
  var headroom, initChangeColors, initScrollReveal, initSwitchScheme, shuffle, toggleClass;

  shuffle = function(o) {
    var i, j, x;
    j = void 0;
    x = void 0;
    i = o.length;
    while (i) {
      j = parseInt(Math.random() * i);
      x = o[--i];
      o[i] = o[j];
      o[j] = x;
    }
    return o;
  };

  headroom = function() {
    var el;
    el = document.querySelector(".mainHeader");
    if (typeof el !== 'undefined' && el !== null) {
      headroom = new Headroom(el);
      headroom.init();
    }
  };

  initScrollReveal = function() {
    var fooReveal;
    window.sr = ScrollReveal();
    fooReveal = {
      delay: 50,
      distance: '40px',
      scale: 1.0,
      easing: 'ease-in'
    };
    sr.reveal('.scrollReveal', fooReveal);
  };

  toggleClass = function(element, className) {
    var classString, nameIndex;
    if (!element || !className) {
      return;
    }
    classString = element.className;
    nameIndex = classString.indexOf(className);
    if (nameIndex === -1) {
      classString += ' ' + className;
    } else {
      classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length);
    }
    element.className = classString;
  };

  initChangeColors = function() {
    var el;
    console.log('test');
    el = document.getElementById('js-switchColors');
    el.addEventListener('click', function() {
      initSwitchScheme();
    });
  };

  initSwitchScheme = function() {
    var classAdded, el, k, len, scheme, schemesArr;
    schemesArr = ["primary", "secondary", "tertiary", "quaternary", "quinary"];
    el = document.getElementById('js-switchTheme');
    shuffle(schemesArr);
    for (k = 0, len = schemesArr.length; k < len; k++) {
      scheme = schemesArr[k];
      if (el.classList.contains(scheme)) {
        el.classList.remove(scheme);
        console.log(scheme);
      } else if (!classAdded) {
        el.classList.add(scheme);
        console.log(scheme);
        classAdded = true;
      }
    }
  };

  (function() {
    initSwitchScheme();
    window.onload = function() {
      initChangeColors();
    };
  })();

}).call(this);
