(function() {
  var headroom, initChangeColors, initScrollReveal, initSwitchScheme, initToggleWorkMenu, shuffle, switchTheme;

  shuffle = function(o) {
    var i, j, x;
    j = void 0;
    x = void 0;
    i = o.length;
    while (i) {
      j = parseInt(Math.random() * i);
      x = o[--i];
      o[i] = o[j];
      o[j] = x;
    }
    return o;
  };

  headroom = function() {
    var el;
    el = document.querySelector(".mainHeader");
    if (typeof el !== 'undefined' && el !== null) {
      headroom = new Headroom(el);
      headroom.init();
    }
  };

  initScrollReveal = function() {
    var fooReveal;
    window.sr = ScrollReveal();
    fooReveal = {
      delay: 50,
      distance: '40px',
      scale: 1.0,
      easing: 'ease-in'
    };
    sr.reveal('.scrollReveal', fooReveal);
  };

  switchTheme = function() {
    initSwitchScheme();
  };

  initChangeColors = function() {
    var el, i;
    el = document.getElementsByClassName('js-switchColors');
    i = 0;
    while (i < el.length) {
      el[i].addEventListener('click', switchTheme, false);
      i++;
    }
  };

  initToggleWorkMenu = function() {
    var el, i, mainNav, navLinks, overlay;
    overlay = document.getElementById('js-overlay');
    navLinks = document.getElementsByClassName("overlayMenu__a");
    i = 0;
    while (i < navLinks.length) {
      navLinks[i].addEventListener('click', function(e) {
        var goTo;
        console.log('clicked');
        e.preventDefault();
        goTo = this.getAttribute("href");
        setTimeout((function() {
          window.location = goTo;
        }), 1000);
      });
      i++;
    }
    el = document.getElementById('js-menu');
    if (typeof el !== 'undefined' && el !== null) {
      mainNav = el.parentNode.parentElement.parentElement.parentElement;
      el.addEventListener('click', function() {
        mainNav.classList.toggle('open');
        if (el.innerHTML === "Projects") {
          el.innerHTML = "Close";
        } else if (el.innerHTML === "Close") {
          el.innerHTML = "Projects";
        } else {
          el.innerHTML = "Close";
        }
        return overlay.classList.toggle('open');
      });
      overlay.addEventListener('click', function(e) {
        overlay.classList.remove('open');
        mainNav.classList.remove('open');
        el.innerHTML = 'Projects';
        e.stopPropagation();
        return false;
      });
    }
  };

  initSwitchScheme = function() {
    var classAdded, el, k, len, scheme, schemesArr;
    schemesArr = ["primary", "secondary", "tertiary", "quaternary", "quinary"];
    el = document.getElementById('js-switchTheme');
    shuffle(schemesArr);
    for (k = 0, len = schemesArr.length; k < len; k++) {
      scheme = schemesArr[k];
      if (el.classList.contains(scheme)) {
        el.classList.remove(scheme);
      } else if (!classAdded) {
        el.classList.add(scheme);
        classAdded = true;
      }
    }
  };

  (function() {
    initSwitchScheme();
    headroom();
    initScrollReveal();
    smoothScroll.init();
    window.onload = function() {
      initChangeColors();
      return initToggleWorkMenu();
    };
  })();

}).call(this);
