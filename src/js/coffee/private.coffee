shuffle = (o) ->
  j = undefined
  x = undefined
  i = o.length
  while i
    j = parseInt(Math.random() * i)
    x = o[--i]
    o[i] = o[j]
    o[j] = x
   o


# isInViewport = (element) ->
#   rect = element.getBoundingClientRect()
#   html = document.documentElement
#   rect.top >= 0 and rect.left >= 0 and rect.bottom <= (window.innerHeight or html.clientHeight) and rect.right <= (window.innerWidth or html.clientWidth)

#attach to each img element
 

headroom = () ->
  el = document.querySelector(".mainHeader")
  if typeof el != 'undefined' and el != null
    headroom  = new Headroom(el)
    headroom.init()
  return

initScrollReveal = () ->
  window.sr = ScrollReveal();
  fooReveal = {
    delay    : 50,
    distance : '40px',
    scale: 1.0,
    easing   : 'ease-in',
  }
  sr.reveal('.scrollReveal', fooReveal);
  return
  
switchTheme = ->
  initSwitchScheme()
  return

# initProjectNav = ()->
#   projects = [
#     'mpol.html'
#     'family-hub.html'
#     'mini-deloitte.html'
#     'inclusion.html'
#     'deloitte-talent-platform.html'
#   ]
#   projectsTotal = projects.length - 1;
#   path = document.location.pathname;
#   file = path.substr(path.lastIndexOf('/')).substr(1);
#   CurrentProjectOrder = projects.indexOf(file);

#   ProjectNavNext = () ->
#     nextProject = document.getElementById('nextProject');
#     if CurrentProjectOrder < projectsTotal
#       NextProjectUrl = projects[CurrentProjectOrder + 1]
#       nextProject.href = NextProjectUrl
#     else if CurrentProjectOrder >= projectsTotal
#       NextProjectUrl = projects[0]
#       nextProject.href = NextProjectUrl
#     return

#   ProjectNavPrev = ()->
#     prevProject = document.getElementById('prevProject');
#     console.log(prevProject)
#     # check current project to ensure continous loop
#     if CurrentProjectOrder <= 0
#       # if less than zero, return to last project
#       PrevProjectUrl = projects[projectsTotal]
#       prevProject.href = PrevProjectUrl
#     else if CurrentProjectOrder > 0
#       # set to previous project in array
#       PrevProjectUrl = projects[CurrentProjectOrder - 1]
#       prevProject.href  =  PrevProjectUrl
#     return
#   ProjectNavPrev();
#   ProjectNavNext();
#   return
# playVideo = () ->
#   # document.getElementById('floorVideo-js').play();
#   # document.getElementById('quizVideo-js').play();


initChangeColors = () ->
  el = document.getElementsByClassName('js-switchColors');

  i = 0
  while i < el.length
    el[i].addEventListener 'click', switchTheme, false
    i++

  # console.log el
  # el.addEventListener 'click', (event)->
  #   console.log "hue has been clicked"
  #   initSwitchScheme()
  #   return
  return

# getComputedTranslateY = (obj) ->
#   if !window.getComputedStyle
#     return
#   style = getComputedStyle(obj)
#   transform = style.transform or style.webkitTransform or style.mozTransform
#   mat = transform.match(/^matrix3d\((.+)\)$/)
#   if mat
#     return parseFloat(mat[1].split(', ')[13])
#   mat = transform.match(/^matrix\((.+)\)$/)
#   if mat then parseFloat(mat[1].split(', ')[5]) else 0

initToggleWorkMenu = () ->
  overlay = document.getElementById('js-overlay')
  # st = window.getComputedStyle(overlay, null);
  # tr = st.getPropertyValue("-webkit-transform") ||
  #        st.getPropertyValue("-moz-transform") ||
  #        st.getPropertyValue("-ms-transform") ||
  #        st.getPropertyValue("-o-transform") ||
  #        st.getPropertyValue("transform") ||
  #        "FAIL";
  # console.log tr;
  navLinks = document.getElementsByClassName("overlayMenu__a")

  i = 0;
  while i < navLinks.length
    navLinks[i].addEventListener 'click', (e) ->
      console.log ('clicked');
      e.preventDefault();
      goTo = this.getAttribute("href")
      setTimeout (->
        window.location = goTo
        return
        ), 1000
      return
    i++
  # navLinks.addEventListener 'click', (e) ->
  #   e.preventDefault();
  #   goTo = this.getAttribute("href")
  #   console.log "NAV CLICKED"
  #   setTimeout (->
  #     window.location = goTo
  #     return
  #   ), 3000
  #   return

  el = document.getElementById('js-menu')
  if typeof el != 'undefined' and el != null
    mainNav = el.parentNode.parentElement.parentElement.parentElement;
    el.addEventListener 'click', () ->

      mainNav.classList.toggle('open')
      if el.innerHTML == "Projects"
        el.innerHTML = "Close"
      else if el.innerHTML == "Close"
        el.innerHTML = "Projects"
      else 
        el.innerHTML = "Close"
      overlay.classList.toggle('open')
      # console.log (getComputedTranslateY(overlay))
      # overlay.addEventListener 'webkitTransitionEnd', (->
      #   if getComputedTranslateY(overlay) >= 0
      #     overlay.classList.add('transitionEnd')
      #   return 
      # ), false
      # return
     overlay.addEventListener 'click', (e)  ->
      # console.log "close has been clicked"
      # conslole.log ("close value " + getComputedTranslateY(overlay))
      overlay.classList.remove('open')
      mainNav.classList.remove('open')
      el.innerHTML='Projects'
      e.stopPropagation();
      # overlay.classList.remove('transitionEnd')
      return false
  return

initSwitchScheme = () -> 
  # list of color names
  schemesArr = ["primary", "secondary", "tertiary", "quaternary", "quinary" ]
  el = document.getElementById('js-switchTheme');
  # shuffle array so color isn't the same everytime
  shuffle schemesArr
  for scheme in schemesArr
    if el.classList.contains(scheme) 
      # toggleClass(el, schemesArr[i])
      el.classList.remove(scheme)
  
    # else if not el.classList.contains(schemesArr[i])
    else if not classAdded
      el.classList.add(scheme)
      classAdded = true

  # switch class every so often
  return

do ->
  initSwitchScheme()
  headroom();
  initScrollReveal();
  smoothScroll.init();
  # hideBorders();
  window.onload = () -> 
    initChangeColors();
    initToggleWorkMenu();

    # imageSlideIn();
    # playVideo();
    # initProjectNav();
  return