#   return !! document.createElementNS && !! document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect;  

# svgFallback = () ->
#   if not supportsSvg()
#     $('img[src$=".svg"]').each( -> 
#       $(this).attr('src', $(this).attr('src').replace(/svg$/, 'png'))
#       return
#       )
#   return

shuffle = (o) ->
  j = undefined
  x = undefined
  i = o.length
  while i
    j = parseInt(Math.random() * i)
    x = o[--i]
    o[i] = o[j]
    o[j] = x
   o

headroom = () ->
  el = document.querySelector(".mainHeader")
  if typeof el != 'undefined' and el != null
    headroom  = new Headroom(el)
    headroom.init()
  return

initScrollReveal = () ->
  window.sr = ScrollReveal();
  fooReveal = {
    delay    : 50,
    distance : '40px',
    scale: 1.0,
    easing   : 'ease-in',
  }
  sr.reveal('.scrollReveal', fooReveal);
  return

# switchLang = (el, btn)->
#   if el.getAttribute("data-language") is "eng" 
#     request.open 'Get', 'index-deu.html'
#     console.log "translate to english clicked"
#     el.setAttribute("data-language", "deu")
#     request.send()
#   else if el.getAttribute("data-language") is "deu"
#     request.open 'Get', 'index-eng.html'
#     console.log "translate to english clicked"
#     el.setAttribute("data-language", "eng")
#     request.send()
# return

# translation = () ->
#   el = document.getElementById("intro-js")
#   if typeof el != 'undefined' and el != null
#     btn = document.getElementById('js-translate')
#     request = new XMLHttpRequest
#     request.onreadystatechange = ->
#       if request.readyState == 4
#         if request.status == 200
#           el.innerHTML = request.responseText
#         else
#           el.innerHTML = 'An error occurred during your request: ' + request.status + ' ' + request.statusText
#       return
    
#    # console.log btn 
#     # console.log deuBtn

#     btn.addEventListener 'click', ->
#       if el.getAttribute('data-language') is 'eng' and btn.getAttribute('data-language') is "deu"
#         request.open 'Get', 'index-deu.html'
#         console.log "translate to German clicked"
#         el.setAttribute("data-language", "deu")

#         # change to eng
#         btn.setAttribute("data-language", "eng")
#         btn.innerHTML = "ENG"     
#         request.send()
#       else if el.getAttribute('data-language') is 'deu' and btn.getAttribute('data-language') is "eng"
#         request.open 'Get', 'index-eng.html'
#         console.log "translate to German clicked"
#         el.setAttribute("data-language", "eng")

#         # change to eng
#         btn.setAttribute("data-language", "deu")
#         btn.innerHTML = "DEU"     
#         request.send()
#       # console.log "btn clicked"
#       # request.open 'Get', 'index-deu.html'
#       # console.log "translate to English clicked"
#       # #change body to english when
#       # el.setAttribute("data-language", "eng")
#       # # btn.setAttribute("data-language", "deu")
#       # btn.innerHTML = "deu"
#       # request.send()
#       return
#   return
  
toggleClass = (element, className) ->
  if !element or !className
    return
  classString = element.className
  nameIndex = classString.indexOf(className)
  if nameIndex == -1
    classString += ' ' + className
  else
    classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length)
  element.className = classString
  return


initChangeColors = () ->
  console.log('test')
  el = document.getElementById('js-switchColors');
  el.addEventListener 'click', ->
    # console.log "hue has been clicked"
    initSwitchScheme()
    return
  return


initSwitchScheme = () -> 
   # list of color names
  schemesArr = ["primary", "secondary", "tertiary", "quaternary", "quinary" ]
  el = document.getElementById('js-switchTheme');
  # shuffle array so color isn't the same everytime
  shuffle schemesArr
  # console.log(schemesArr[0]);

  # append class to html element

  #check if classname has been added
  # # if added remove it 
  # i = schemesArr.length - 1
  # while i >= 0
  #   schemesArr[i]
  #   i--

  # el.classList.add(schemesArr[0])

  for scheme in schemesArr
    if el.classList.contains(scheme) 
      #toggleClass(el, schemesArr[i])
      el.classList.remove(scheme)
      console.log(scheme)
  
    # else if not el.classList.contains(schemesArr[i])
    else if not classAdded
      el.classList.add(scheme)
      console.log(scheme)
      classAdded = true


  # switch class every so often
  return

# isTouchDevice = () -> 
#   "ontouchstart" of window or window.DocumentTouch and document instanceof DocumentTouch 

# initMobileHover = () ->
#   if !isTouchDevice()
#     # svg = document.getElementById("self-portrait"); 
#     # svgDoc = svg.contentDocument;
#     # headShape = svgDoc.getElementById('headshape');
#     # touchElement.addEventlistener('click', function() {
#       # toggleClass(touchElement, 'touch');
#     # });
#     # console.log 'headshape: ' + headshape

#     # a = document.getElementById('svg')
#     # a.addEventListener 'load', (->
#     #   svgDoc = a.contentDocument
#     #   el = svgDoc.getElementById('head')
#     #   el.addEventListener 'click', (->
#     #     console.log 'clicked'
#     #     return
#     #   ), false
#     #   return
#     # ), false


#     # touchElement = document.getElementsByTagName('svg');
#     # console.log("touch element " + touchElement);

#     # document.getElementsByTagName('svg').addEventListener 'click', ->
#     #   toggleClass document.getElementsByTagName 'svg', 'touch'
#     #   return
#     # touchElement = document.getElementById('svg');
#     # touchElement.addEventlistener 'click', ()->
#       # toggleClass(touchElement, 'touch') 
#       # return     
#    return
# ---


do ->
  initSwitchScheme()
  window.onload = () -> 
    initChangeColors();
    # translation();
    return
  return