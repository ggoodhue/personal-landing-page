module.exports = (grunt) ->
  _ = grunt.util._
  path = require("path")
  config = _.extend({}, require("load-grunt-config")(grunt,
    configPath: path.join(__dirname, "tasks/options")
    loadGruntTasks: false
    init: false
  
  # tasks have precedence
  ), require("load-grunt-config")(grunt,
    configPath: path.join(__dirname, "tasks")
    init: false
  ))
  
  # Loads tasks in `tasks/` folder
  grunt.loadTasks "tasks"
  grunt.initConfig config
  return